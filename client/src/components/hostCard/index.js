import { Component } from 'preact';
import axios from 'axios';
import Card from 'preact-material-components/Card';
import List from 'preact-material-components/List';
import style from './style';
import cx from 'classnames';

export default class HostCard extends Component {
	constructor(props) {
		super(props);
		this.state = { hosts: [] };
	}

	componentWillMount() {
		axios.get(`http://0.0.0.0:5000/api/applications/${this.props.hostId}`).then((result) => {
			if (result.status === 200 && result.data.length > 0) {
				this.setState({ hosts: result.data.slice(0, 5) });
			}
		}).catch((error) => {
			console.log(error);
		});
	}

	renderHosts() {
		return this.state.hosts.map((host) => <List.Item>{host.apdex} - {host.name}</List.Item>);
	}

	render() {
    let className = cx(style.default, {
      [style.cardGrid]: !this.props.displayList,
      [style.cardList]: this.props.displayList
    });
		return (
			<Card class={className}>
				<div class={style.cardHeader}>
					<h2 class=" mdc-typography--title">{this.props.hostId}</h2>
				</div>
				<div class={style.cardBody}>
					<List>
						{this.renderHosts()}
					</List>
				</div>
			</Card>
		);
	}
}
