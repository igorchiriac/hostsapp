import { Component } from 'preact';
import 'preact-material-components/Card/style.css';
import 'preact-material-components/Button/style.css';
import 'preact-material-components/Checkbox/style.css';
import Checkbox from 'preact-material-components/Checkbox';
import style from './style';
import HostCard from '../../components/hostCard/index';

export default class Home extends Component {
	constructor(props) {
		super(props);
		this.state = { hosts: [], list: true };
	}

  onChange = (e) => {
	  this.setState({ checked: e.target.checked });
  };

	render() {
		return (
		  <div>
        <h1>Apps by Host</h1>
        <Checkbox onChange={this.onChange} checked={this.state.checked} />
        <span>Show as a list</span>
        <div class={style.home}>
          <HostCard hostId={'b0b655c5-928a.nadia.biz'} displayList={this.state.checked} />
          <HostCard hostId={'95b346a0-17f4.abbigail.name'} displayList={this.state.checked} />
          <HostCard hostId={'e0419f48-6a5a.craig.info'} displayList={this.state.checked} />
          <HostCard hostId={'92116865-5462.conor.com'} displayList={this.state.checked} />
        </div>
      </div>
		);
	}
}
