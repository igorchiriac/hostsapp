import * as shell from "shelljs";

shell.cp("-R", "src/assets/", "dist/assets/");
shell.cp("src/configurations/config.dev.json", "dist/configurations/");
