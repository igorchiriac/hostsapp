import * as Hapi from "hapi";
import * as Joi from "joi";
import ApplicationController from "./application-controller";
import { IServerConfigurations } from "../configurations";

export default function (server: Hapi.Server, configs: IServerConfigurations) {

    const applicationController = new ApplicationController(configs);
    server.bind(applicationController);

    server.route({
        method: 'GET',
        path: '/applications/{hostId}',
        config: {
            handler: applicationController.getTopAppsByHost,
            tags: ['api', 'applications'],
            description: 'Get top application by host id.',
            validate: {
                params: {
                    hostId: Joi.string().required()
                },
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Application founded.'
                        },
                        '404': {
                            'description': 'Application does not exists.'
                        }
                    }
                }
            }
        }
    });
}