import * as Hapi from 'hapi';
import * as Boom from 'boom';
import {IServerConfigurations} from '../configurations';
import {IRequest} from '../interfaces/request';

export default class ApplicationController {
    private configs: IServerConfigurations;

    constructor(configs: IServerConfigurations) {
        this.configs = configs;
    }

    public async getTopAppsByHost(request: IRequest, h: Hapi.ResponseToolkit) {
        let hostId = request.params['hostId'];

        const quickSort = list => {
            if (list.length < 2) {
                return list;
            }
            let pivot = list[0];
            let left = [];
            let right = [];
            for (let i = 1, total = list.length; i < total; i++) {
                if (list[i].apdex < pivot.apdex) {
                    left.push(list[i]);
                } else {
                    right.push(list[i]);
                }
            }
            return [
                ...quickSort(right),
                pivot,
                ...quickSort(left)
            ];
        };

        const data = require('../assets/host-app-data.json');
        const appsForHost = data.filter((app) => {
            return app.host.indexOf(hostId) !== -1;
        });

        const sortedApps = quickSort(appsForHost);

        if (appsForHost.length) {
            return sortedApps.slice(0, 25);
        } else {
            return Boom.notFound();
        }
    }
}